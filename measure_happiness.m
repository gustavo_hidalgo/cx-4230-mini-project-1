function matrixData = measure_happiness( inputMatrix, affinityTowardsOthersMatrix )
    inputSize = size(inputMatrix);
    
    %change data to happiness value of each person in the for loop
    matrixData = rand(inputSize(1), inputSize(2));
    
    for i=1:inputSize(1)
        for j=1:inputSize(2)
            
            myPeople = inputMatrix(i, j);
            
            %am I attracted toward my opposite?
            PROB_DIFF_MINDED_HAPPINESS = 1 - 0.25;
            if affinityTowardsOthersMatrix(i,j) <= PROB_DIFF_MINDED_HAPPINESS
                myPeople = (mod(myPeople, 2)) + 1;
            end
            
            iFeel = 1; %happiness value of a particular person (i, j)
            
            %don't go out of bounds of the array when measuring neighbors
            
            if i>1
                if inputMatrix(i-1, j) == myPeople
                    iFeel= iFeel+1;
                end
                
                if (inputMatrix(i-1, j) ~= myPeople) && (inputMatrix(i-1, j) ~= 0)
                    iFeel = iFeel-1;
                end
            end
            
            
            
            if i< inputSize(1)
                if inputMatrix(i+1, j) == myPeople
                    iFeel= iFeel+1;
                end
                
                if (inputMatrix(i+1, j) ~= myPeople) && (inputMatrix(i+1, j) ~= 0)
                    iFeel = iFeel-1;
                end
            end
            
            
            
            if j>1
                if inputMatrix(i, j-1) == myPeople
                    iFeel= iFeel+1;
                end
                
                if (inputMatrix(i, j-1) ~= myPeople) && (inputMatrix(i, j-1) ~= 0)
                    iFeel = iFeel-1;
                end
            end
            
            if j < inputSize(2)
                if inputMatrix(i, j+1) == myPeople
                    iFeel= iFeel+1;
                end
                
                if (inputMatrix(i, j+1) ~= myPeople) && (inputMatrix(i, j+1) ~= 0)
                    iFeel = iFeel-1;
                end
            end
            
            
            matrixData(i, j) = iFeel;
            
            if inputMatrix(i, j) == 0
                matrixData(i, j) = 0;
            end
        end
    end
end

