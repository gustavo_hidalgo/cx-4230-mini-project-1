function [newWorldMatrix, moves] = relocate_cells(worldMatrix, happiness)
moves = 0;
for r = 1:size(worldMatrix, 1)
    for c = 1:size(worldMatrix, 2)
        if(happiness(r,c) < 0 && worldMatrix(r,c) > 0)
           moves = moves + 1;
           [emptyRows, emptyColumns] = find(worldMatrix == 0);
           new = randi(length(emptyRows));
           emptyPlace = [emptyRows(new) emptyColumns(new)];
           worldMatrix(emptyPlace(1), emptyPlace(2)) = worldMatrix(r,c);
           worldMatrix(r,c) = 0;
        end
    end
end
newWorldMatrix = worldMatrix;
end
