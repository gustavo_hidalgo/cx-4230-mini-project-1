function returnMatrix = neighborhood_color(inputMatrix)
    inputSize = size(inputMatrix);
    returnMatrix = zeros(inputSize(1), inputSize(2));
    
    aroundColumnsOffset = [-1, 0, 1, -1, 0, 1, -1, 0, 1];
    aroundRowsOffset = [-1, -1, -1, 0, 0, 0, 1, 1, 1];
    
    for m = 1:(inputSize(1))
        for n = 1:(inputSize(2))
            if inputMatrix(m, n) == 0
                continue
            end
            
            averageColor = 0;
            colorsAdded = 0;
            for offset = 1:9
                if m + aroundColumnsOffset(offset) >= 1 && m + aroundColumnsOffset(offset) <= inputSize(1) ...
                   && n + aroundRowsOffset(offset) >= 1 && n + aroundRowsOffset(offset) <= inputSize(2)
                    if inputMatrix(m + aroundColumnsOffset(offset), n + aroundRowsOffset(offset)) ~= 0
                        averageColor = averageColor + inputMatrix(m + aroundColumnsOffset(offset), n + aroundRowsOffset(offset));
                        colorsAdded = colorsAdded + 1;
                    end
                end
            end
            if colorsAdded ~= 0
                averageColor = averageColor / colorsAdded;
            end
            
            if averageColor > (2 + 1) / 2
                returnMatrix(m, n) = 2;
            else
                returnMatrix(m, n) = 1;
            end
        end
    end
    
    for m = 1:(inputSize(1))
        for n = 1:(inputSize(2))
            if inputMatrix(m, n) ~= 0
                continue
            end
            
            averageColor = 0;
            colorsAdded = 0;
            for offset = 1:9
                if m + aroundColumnsOffset(offset) >= 1 && m + aroundColumnsOffset(offset) <= inputSize(1) ...
                   && n + aroundRowsOffset(offset) >= 1 && n + aroundRowsOffset(offset) <= inputSize(2)
                    if inputMatrix(m + aroundColumnsOffset(offset), n + aroundRowsOffset(offset)) ~= 0
                        averageColor = averageColor + inputMatrix(m + aroundColumnsOffset(offset), n + aroundRowsOffset(offset));
                        colorsAdded = colorsAdded + 1;
                    end
                end
            end
            if colorsAdded ~= 0
                averageColor = averageColor / colorsAdded;
            end
            
            if averageColor == 0
                returnMatrix(m, n) = 0;
            elseif averageColor > (2 + 1) / 2
                returnMatrix(m, n) = 2;
            else
                returnMatrix(m, n) = 1;
            end
        end
    end
end