function segregationPercentage = measure_segregation(neighborhoodColorMatrix, redProbability, blueProbability)
    neighborhoodSize = size(neighborhoodColorMatrix);
   
    iterations = 1;
    tempSize = min(neighborhoodSize(1), neighborhoodSize(2));
    while (tempSize > 0)
        tempSize = idivide(tempSize, int32(2), 'floor');
        iterations = iterations + 1;
    end
    
    currentIteration = 2;
    stepSize = 2^(currentIteration - 1);
    tempNeighborhoodColorData = cell(1);
    tempNeighborhoodColorData{1} = neighborhoodColorMatrix; % this would be the first iteration
    while stepSize < neighborhoodSize(1) || stepSize < neighborhoodSize(2)
        previousDataSize = size(tempNeighborhoodColorData{currentIteration - 1});
        tempNeighborhoodColorData{currentIteration} = zeros(idivide(previousDataSize(1) - 1, int32(2), 'floor') + 1, ...
                                                            idivide(previousDataSize(2) - 1, int32(2), 'floor') + 1);
        currentDataSize = size(tempNeighborhoodColorData{currentIteration});
        n = 1;
        while n <= currentDataSize(2)
            m = 1;
            while m <= currentDataSize(1)
                accumulatingNeighborhoodColor = 0;
                numberAveraging = 0;
                for in_n = 0:1
                    for in_m = 0:1
                        if 2 * m + in_m - 1 > previousDataSize(1) || 2 * n + in_n - 1 > previousDataSize(2)
                            continue
                        end
                        if tempNeighborhoodColorData{currentIteration - 1}(2 * m + in_m - 1, 2 * n + in_n - 1) ~= 0
                            accumulatingNeighborhoodColor = accumulatingNeighborhoodColor ...
                                + tempNeighborhoodColorData{currentIteration - 1}(2 * m + in_m - 1, 2 * n + in_n - 1);
                            numberAveraging = numberAveraging + 1;
                        end
                    end
                end
                if numberAveraging == 0
                    tempNeighborhoodColorData{currentIteration}(m, n) = 0;
                else
                    tempNeighborhoodColorData{currentIteration}(m, n) = round(accumulatingNeighborhoodColor / numberAveraging);
                end
            m = m + 1; % stepSize;
            end
        n = n + 1; % stepSize;
        end
        currentIteration = currentIteration + 1;
        stepSize = 2^(currentIteration - 1);
    end
    
    scalar = 0;
    tempNeighborhoodColorDataSize = size(tempNeighborhoodColorData);
    for n = 1:tempNeighborhoodColorDataSize(2)
        % currentMacroneighborhoodSize = size(tempNeighborhoodColorData{n});
        % scalar = scalar + currentMacroneighborhoodSize(1);
        % scalar = scalar + (2^n);
        scalar = scalar + n;
    end    
    % scalar = sum(1:tempNeighborhoodColorDataSize(2));
    
    segregationPercentage = 0;
    
    % version 1
    redTotals = zeros(tempNeighborhoodColorDataSize(2), 1);
    blueTotals = zeros(tempNeighborhoodColorDataSize(2), 1);
    for index = 1:tempNeighborhoodColorDataSize(2)
        arraySize = size(tempNeighborhoodColorData{index});
        for n = 1:arraySize(2)
            for m = 1:arraySize(1)
                if tempNeighborhoodColorData{index}(m, n) == 1
                    redTotals(index, 1) = redTotals(index) + 1;
                elseif tempNeighborhoodColorData{index}(m, n) == 2
                    blueTotals(index, 1) = blueTotals(index) + 1;
                end
            end
        end
        
        segregationPercentage = segregationPercentage ...
            + ((tempNeighborhoodColorDataSize(2) - index + 1) / scalar) * (sqrt((blueTotals(index, 1) / (arraySize(1) * arraySize(2)) - blueProbability)^2) ...
            + sqrt((redTotals(index, 1) / (arraySize(1) * arraySize(2)) - redProbability)^2)) / 2;
    end
end