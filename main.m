% CX 4230 MP-1 Part B
% Daniel Dias, Kristian Eberhardson, Gustavo Hidalgo
% main segregation algorithm cellular automata model

% constants

M = 100;
N = 100;

EMPTY = 0;
RED = 1;
BLUE = 2;

RED_PROBABILITY = 0.6;
BLUE_PROBABILITY = 0.2;

% setup

needToMove = true;

worldMatrix = rand(M, N);
for m = 1:M
    for n = 1:N
        if worldMatrix(m, n) > (1 - BLUE_PROBABILITY)
            worldMatrix(m, n) = BLUE;
        elseif worldMatrix(m, n) <= (1 - BLUE_PROBABILITY) && ...
               worldMatrix(m, n) > (1 - BLUE_PROBABILITY - RED_PROBABILITY)
            worldMatrix(m, n) = RED;
        else
            worldMatrix(m, n) = EMPTY;
        end
    end
end

% actual simulation
affinityForOthers = rand(M,N)*2;
%affinityForOthers = ones(M,N);

axis square
while needToMove
    neighborhoodColorMatrix = neighborhood_color(worldMatrix);
    [worldMatrix, moves] = relocate_cells(worldMatrix, measure_happiness(worldMatrix, affinityForOthers));
    subplot(1, 2, 1)
    imagesc(worldMatrix);
    colorbar
    subplot(1, 2, 2)
    imagesc(neighborhoodColorMatrix)
    pause(0.1);
    needToMove = moves ~= 0;
end

segregationPercentage = measure_segregation(neighborhoodColorMatrix, RED_PROBABILITY, BLUE_PROBABILITY);

subplot(1, 2, 1)
imagesc(worldMatrix)
title('World')
colormap([1, 1, 1; 0, 0.6, 0; 0, 0, 1])
colorbar

subplot(1, 2, 2)
imagesc(neighborhoodColorMatrix)
title('Neighborhood Color')
colormap default
colorbar

display(segregationPercentage)